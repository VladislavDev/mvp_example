//
//  PresenterTests.swift
//  MVP_Tests
//
//  Created by Vlad on 8/10/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import XCTest
import UIKit
@testable import MVP_Example

class MockView: ViewProtocol {
    var label: UILabel
    
    init() {
        label = UILabel()
        label.text = "My Label"
    }
}

class PresenterTests: XCTestCase {
    var viewController: ViewProtocol!
    var model: Model!
    var presenter: Presenter!
    
    override func setUp() {
        viewController = MockView()
        model = Model(name: "Buzz", age: 24)
        presenter = Presenter(vc: viewController, model: model)
    }
    
    override func tearDown() {
        viewController = nil
        model = nil
        presenter = nil
    }

    func testModuleIsCreated() {
        XCTAssertNotNil(viewController, "ViewController is not nil")
        XCTAssertNotNil(model, "Model is not nil")
        XCTAssertNotNil(presenter, "Presenter is not nil")
    }
    
    func testChangeLabel() {
        presenter.changeLabel()
        
        XCTAssertEqual(viewController.label.text, "New text value")
    }
}
