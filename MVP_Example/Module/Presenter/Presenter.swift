//
//  Presenter.swift
//  MVP_Example
//
//  Created by Vlad on 8/10/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

// MARK: - Protocols, нужны чтобы соблюсти DIP
protocol ViewProtocol {
    var label: UILabel { get set }
}

protocol PresenterProtocol {
    // Делаем Dependency Injection контроллера и данных
    init(vc: ViewProtocol, model: Model)
}

// MARK: - Presenter
class Presenter: PresenterProtocol {
    private var vc: ViewProtocol
    private var model: Model
    
    required init(vc: ViewProtocol, model: Model) {
        self.vc = vc
        self.model = model
    }
    
    func printDataToConsole() {
        print("Data: \(String(describing: model.name!)) \(String(describing: model.age!))")
    }
    
    func changeLabel() {
        vc.label.text = "New text value"
    }
}
