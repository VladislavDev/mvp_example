//
//  VC.swift
//  MVP_Example
//
//  Created by Vlad on 8/10/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class VC: UIViewController, ViewProtocol {
    // MARK: - Presenter
    var presenter: Presenter?
    
    // MARK: - Methods for working with Presenter
    @objc private func actionPrintToConsole() {
        presenter?.printDataToConsole()
    }
    
    @objc private func changeLabel() {
        presenter?.changeLabel()
    }
    
    // MARK: - ViewController settings
    var label: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "That is label"
        return l
    }()
    
    var btnPrint: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Print", for: .normal)
        b.backgroundColor = .black
        
        b.addTarget(self, action: #selector(actionPrintToConsole), for: .touchUpInside)
        
        return b
    }()
    
    var btnChangeLabel: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Change label text", for: .normal)
        b.backgroundColor = .black
        
        b.addTarget(self, action: #selector(changeLabel), for: .touchUpInside)
        
        return b
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        setupLabel()
        setupBtnPrint()
        setupChangeLabelBtn()
    }
    
    private func setupLabel() {
        self.view.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
    }
    
    private func setupBtnPrint() {
        self.view.addSubview(btnPrint)
        
        NSLayoutConstraint.activate([
            btnPrint.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 15),
            btnPrint.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
    }
    
    private func setupChangeLabelBtn() {
        self.view.addSubview(btnChangeLabel)
        
        NSLayoutConstraint.activate([
            btnChangeLabel.topAnchor.constraint(equalTo: btnPrint.bottomAnchor, constant: 15),
            btnChangeLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
    }
}

