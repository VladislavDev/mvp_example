//
//  Model.swift
//  MVP_Example
//
//  Created by Vlad on 8/10/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

struct Model {
    let name: String?
    let age: Int?
}
