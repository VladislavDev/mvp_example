//
//  ModuleBuilder.swift
//  MVP_Example
//
//  Created by Vlad on 8/10/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class ModuleBuilder {
    static func createModule() -> UIViewController {
        let viewController = VC()
        let model = Model(name: "Bill", age: 22)
        let presenter = Presenter(vc: viewController, model: model)
        viewController.presenter = presenter
        
        return viewController
    }
}
